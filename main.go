package main

import (
	"fmt"
	"regexp"
)

func main() {
	n := 3
	arr := []int{1, 25, 7, -7, -3, 12, -22, 22, 0}

	fmt.Printf("Tổng dãy số từ 1 tới n với n bằng %d : %d\n", n, Sum(n))
	fmt.Printf("Sắp xếp dãy số theo chiều tăng dần : %v\n", SortArray(&arr))
	fmt.Printf("Sắp xếp dãy số theo chiều tăng dần của giá trị tuyệt đối : %v\n", SortAbsoluteArray(&arr))

	fmt.Println("Scale image : ", ScaleImage("600x100", "https://cdn.shopify.com/100xMacBook.jpg_3_result_x2.jpg?v=15"))

}

func Sum(n int) int {
	sum := 0
	i := 1
	for i <= n {
		sum += i
		i++
	}
	return sum
}

func SortArray(array *[]int) []int {
	po := *array
	p := 0
	x := 0
	i := 1
	for i < len(po) {
		p = i - 1
		x = po[i]
		for p >= 0 && po[p] > x {
			po[p+1] = po[p]
			p--
		}
		po[p+1] = x
		i++
	}
	return *array
}

func SortAbsoluteArray(array *[]int) []int {
	po := *array
	p := 0
	x := 0
	i := 1
	for i < len(po) {
		p = i - 1
		x = po[i]
		for p >= 0 && (po[p] > x || 0-po[p] > x) {
			po[p+1] = po[p]
			p--
		}
		po[p+1] = x
		i++
	}
	return *array
}

func ScaleImage(size, url string) string {
	regexName, _ := regexp.Compile("(\\S+).jpg(\\S+)")
	regexSize, _ := regexp.Compile("_\\d*x\\d*")
	regexNum, _ := regexp.Compile("[0-9]+")

	arrayMatch := regexName.FindStringSubmatch(url)
	arrayMatchSize := regexSize.FindAllStringSubmatchIndex(
		url, -1)

	if len(arrayMatchSize) > 0 {
		strMatchSize := arrayMatchSize[len(arrayMatchSize)-1]
		preChar := url[(strMatchSize[len(strMatchSize)-2]):(strMatchSize[len(strMatchSize)-2] + 1)]
		befoChar := url[(strMatchSize[len(strMatchSize)-2] + 2):(strMatchSize[len(strMatchSize)-2] + 3)]
		if !regexNum.MatchString(preChar) && !regexNum.MatchString(befoChar) {
			url = arrayMatch[len(arrayMatch)-2] + "_" + size + ".jpg" + arrayMatch[len(arrayMatch)-1]
		} else {
			url = url[:strMatchSize[len(strMatchSize)-2]] + "_" + size + url[strMatchSize[len(strMatchSize)-1]:]
		}
	} else {
		if len(arrayMatch) > 1 {
			url = arrayMatch[len(arrayMatch)-2] + "_" + size + ".jpg" + arrayMatch[len(arrayMatch)-1]
		}
	}

	return url
}
